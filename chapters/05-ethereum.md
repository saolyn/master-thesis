# Ethereum Implementation
Ethereum is built on the blockchain technology just like every other cryptocurrency, however the goal behind its creation is what separates it. As described in Ethereum's white paper, it was developed with the "intent [..] to create an alternative protocol for building decentralized applications"\cite{ethwhitepaper}. It achieves this with a custom blockchain that has a built-in Turing-complete programming language. Thus allowing developers to create decentralized applications that can run on the Ethereum Virtual Machine and be programmed using languages like Solidity.

Ethereum includes its own currency called Ether which "is the main internal crypto-fuel of Ethereum, and is used to pay transaction fees."\cite{ethwhitepaper} In order to interact with the blockchain and send transactions, the fee of the computation is calculated in gas which is paid in Ether. The creator of a transaction can set a gas limit which is the maximum amount of gas that they are willing to spend on this transaction. The amount of gas necessary to verify a transaction is proportional to the amount of code that needs to be executed on it, any unused gas is refunded at the end of the transaction. Spending more per unit of gas can determine how quickly that transaction will be mined.

One of the most revolutionary aspects of Ethereum is how it enables developers to program their own smart contracts using their built-in programming language is regarding the use of smart contracts. A smart contract is written in code such that once the terms of the contract have been met, the deal will be automatically executed. The rules around an agreement are automatically enforced by the contract ensuring that the exchange is conflict free. Thus making smart contracts a secure means to automate exchanges.

## Node
In order to ensure that Go was the primary language used in this tool, a full Ethereum node was set up by running a command line interface called geth which is implemented in Go.

There is an official implementation of Ethereum in Go called Go-Ethereum which is an interface for interacting with Ethereum

With geth it is possible to run a few different kinds of node synchronizations, the first is the full sync which contains historical data such as block headers and bodies as well as validating everything from the original block---also known as the genesis block.

Then there is the fast sync which does not process any transactions except the latest ones. It also contains block headers and block bodies but only for the more recent blocks. No historical data is stored.

Finally the light sync which can only fetch the current state of the network. There is also a chance that the node will only be able to connect to a limited number of peers as only geth nodes that are run with the `--lightserv` flag will be able to connect to them. Nodes running Parity do not support them at all and will not be able to connect to them.

This tool has initially been run on a full node, but since historical data is not necessary---only current data is being gathered and stored---a fast sync is the ideal one to use. The light server would have been a good option as well if it were not for the issue of peer connectivity and the lack of Parity support.

With a fast node running, the tool has an Ethereum specific code that can interact with Geth and the Ethereum network using the JSON-RPC commands which are built into Geth. These include the standard commands as well as those that are Geth specific. As the tool does make use of at least one Geth specific command, it would not work on a Parity node without making some changes to the code.

During the development, the Geth node was run with the following JSON-RPC API options:

```
geth -fast -cache=1024 --rpcapi "eth,net,web3,admin" --maxpeers 150 --rpc
```

\begin{description}
\item[-fast] specifies that the node will be running on fast sync mode and therefore not processing the entire history of the Ethereum network.
\item[--rpcapi] offers the API over the HTTP-RPC interfaces specified within double quotes.
\item[--maxpeers] add a maximum number of peers that can connect at any given time.
\item[-rpc] enables the HTTP-RPC server.
\end{description}

The geth interface can be connected to simply with the host address as can be seen below.

```
client, err := rpc.Dial(config.ConnectRPC.Host)
core.CheckErr("Could not create rpc client: %v", err)
```

## Transactions
With the Geth interface up and running, the tool can be launched. The configuration file is parsed and the data extracted. Connecting to the database and the Ethereum RPC interface is achieved by making use of the server information found in the config file.
When the ticker reaches the desired length of time for polling transactions---also specified in the configuration file---the code creates a new filter.
```
err = client.Call(&transactionFilter, "eth_newPendingTransactionFilter")
```
A filter is created in the node that notifies when a new transaction has arrived, and this filter specifically notifies for incoming pending transactions only. This call does not return any data so in order to be able to collect the hashes of these incoming transactions, another call has to be made.
```
err = client.Call(&transactions, "eth_getFilterChanges", transactionFilter)
```
If there has been a change, this returns an array of transaction hashes, if not then it returns null.
The time at which these transactions were observed is assigned to a variable and passed to the function---`TxsFromEthereum`---that moves this data into a struct of generic type `Transaction` along with the collected hashes and node ID.

```
func TxsFromEthereum (ethTxs []string, timeObs int64, nodeID int) []Transaction {
    var transactions []Transaction
    for _, ethTx := range ethTxs {
        transactions = append(transactions, TxFromEthereum(ethTx, core.Pending, timeObs, nodeID))
    }
    return transactions
}
```
The function above iterates over the list of pending transaction hashes, and normalises each transaction into the generic type `Transaction` by passing the transaction hash, observed time, status---by default it is set to `Pending`---and node ID.
The function below is where the transactions are converted to type `Transaction`. The order in which the variables are listed matches the definition of the struct.
```
func TxFromEthereum (ethTx string, status core.Status, timeObs int64, nodeID int) Transaction {
    return Transaction{ethTx, timeObs, status, "ETH", nodeID}
}
```
Once the transactions are normalised, they are then inserted into the database. This is done for all observed pending transactions.

The filter only fetches pending transactions. Therefore this will not be able to update a transaction when it is assigned to a block. Initially the code would mark a transaction as completed by iterating over each of the transactions in the database and checking whether or not they had been included in a block. However, checking thousands of transactions when only a few of them get included in a block is exceptionally inefficient.
To simplify the way this is done, the code now fetches the transactions directly from the completed block and updates those that are present.

A variable `nextBlock` which will hold the block number of the latest block is initialised to `0`. The latest block is the most recent block to have been added to the longest chain. This is done before the ticker so that once the latest block number is fetched, it can be reused.
Another Ticker is declared for the portion of code responsible for fetching the blocks so that this may occur concurrently with the code responsible for collecting the incoming pending transactions.
The latest block is fetched, and the block number is saved to `latestBlockNumber`
The currently pending block is fetched, and its number is stored in a variable as is shown in the code below.

```
latestBlock := GetBlock(client, "latest")
latestBlockNumber := latestBlock.GetNumber()

if nextBlock == 0 {
    fmt.Print("Getting latest block...")
    nextBlock = latestBlockNumber
}
```

If `nextBlock` is still `0` then the `latestblockNumber` is assigned to it.

```
for nextBlock <= latestBlockNumber {
                block = GetBlock(client, fmt.Sprintf("0x%x", nextBlock))
                timeComp, err := strconv.ParseInt(block.Timestamp, 0, 64)
                core.CheckErr("Can't convert timestamp:", err)

                for _, hash := range block.Transactions {
                    err = client.Call(&transactionInfo, "eth_getTransactionReceipt", hash)
                    if err != nil {
                        fmt.Printf("Can't get transaction receipt: %s\n", err)
                        continue
                    }
                    status := normalizeStatus(transactionInfo.Status)
                    database.UpdateTransaction(hash, config.Node.Currency, status, timeComp, config.Node.ID)
                    fmt.Printf("Tx %s: status = %s\n", hash, status)
                }
                nextBlock = block.GetNumber() + 1
            }
```

While `nextBlock <= latestBlockNumber` the list of transactions from the `nextBlock` are extracted. The condition of the `for` loop ensures that if the program finds itself a couple of blocks behind, it is still able to retrieve and update all of the included transactions.

The block number is converted to hexadecimal and an \gls{rpc} call is made to fetch that block from the blockchain. The data is stored in the variable `block` which is of type `Block` which contains the fields `Number`, `Timestamp` and `Transactions`.
The Timestamp is used as the time that the transactions were completed, For each of the transactions in the blocks list of transactions, the hash is used to recover the transaction receipt from the blockchain.
```
            for _, hash := range block.Transactions {
                err = client.Call(&transactionInfo, "eth_getTransactionReceipt", hash)
                if err != nil {
                    fmt.Printf("Can't get transaction receipt: %s\n", err)
                    continue
                }
```
The transaction receipt contains the completed status of a transaction, `0x1` for success and `0x0` for failed. This can only be fetched if a transaction has been included in a block, if it has not then it returns an error.
```
                status := normalizeStatus(transactionInfo.Status)
                database.UpdateTransaction(hash, config.Node.Currency, status, timeComp)
            }
            nextBlock = block.GetNumber() + 1
        }
```
The status is then normalized using the mapping in the code snippet below. The status codes vary between cryptocurrencies so each currency needs it's own mapping.
The transaction that matches the hash, currency and node ID then has its status updated and the time of completion added. An SQL `UPDATE` query may only be done on one entry at a given time.
Finally `nextBlock` is increased by one and if the condition of the for loop is still `True` the process repeats, if not the loop ends and the channel created by the Ticker is stopped.
```
var statusFromEth = map[string]core.Status {
    "0x1": core.Success,
    "0x0": core.Failed,
}

func normalizeStatus(ethStatus string) core.Status {
return statusFromEth[ethStatus]
}
```

## Peers
In the standard Ethereum JSON-RPC commands there is nothing to allow for the collection of connected peers. However some Geth specific commands are part of the management API that can do just that.

A Ticker ensures that the peers are fetched as often as is declared in the configuration file. We declare a variable `ethPeers` that will hold our list of connected peers. The type of `ethPeers` is a Geth generic type to handle peers.

```
var ethPeers []*p2p.PeerInfo
err = client.Call(&ethPeers, "admin_peers")
core.CheckErr("Can't get list of peers:", err)
```
The `admin_peers` command returns a list of peers and their respective ID's, name, network information---remote and local addresses---and some details about protocols. In the code above, `admin_peers` fetches all the currently connected peers and using a pointer assigns them to `ethPeers`.
The collected peers are then normalised so that each currency has the same format.
```
peers := types.PeersFromEthereum(ethPeers, timeObs, config.Node.ID)
```
The peer's remote address extracted from the Ethereum network is the IP followed by the port. Since only the IP is needed, within the normalisation, the IP and port are separated and the IP stored.

Using the IP address, it is then possible to fetch the AS number associated to that IP range. This is done for each peer which is then converted into the generic type `Peer` in the same way that transactions are handled.  
```
func PeerFromEthereum(ethPeer *p2p.PeerInfo, timeObs int64, nodeID int) Peer {
    remoteAddress, _, err := net.SplitHostPort(ethPeer.Network.RemoteAddress)
    core.CheckErr("Failed to remove port: ", err)

    ASNumber := database.CheckExistingAS(remoteAddress)
    return Peer{ethPeer.Name, remoteAddress, ASNumber, timeObs, "ETH", nodeID}
}

func PeersFromEthereum (ethPeers []*p2p.PeerInfo, timeObs int64, nodeID int) []Peer {
    var peers []Peer
    for _, ethPeer := range ethPeers {
        peers = append(peers, PeerFromEthereum(ethPeer, timeObs, nodeID))
    }
    return peers
}
```
## Issues encountered
The Go-Ethereum node is the first node I had ever set up, so I immediately had issues trying to run the node in a manner that would allow me to extract all the information I needed. I SSH into the machine and through the command line installed Go and the Go-Ethereum repository on my machine. I then began running `geth` as a light node, to see whether I could download the least amount of the blockchain and still extract everything I needed.

While this worked great for extracting transactions, the node was having great difficulty finding peers to connect to At times you would have to wait hours just to be able to connect to two peers at most. Since peers are an important part of the data I hoped to extract, I started running my node with full sync to see how much more data I would be able to observe. I very quickly started to run out of disk space, so my co-supervisor, Pierre-Matthieu, via Bity was able to give me multiple disk space upgrades until finally my Ethereum node was able to fully synchronize with the network.

A full sync node can be run with the `-archive` flag which means that the entire historical data of the blockchain is downloaded on the machine. Since all this data is not actually necessary the nodes be currently running a standard full sync. It could however be possible to run a node using fast sync and still be able to get the same results.

With the node now running, I had to find a way to get it to run without me being always connected through SSH. As soon as I disconnected, the node would stop running causing my data to be inaccurate as there is no current way to save the state of the tool so that it can be restarted where it left off. I attempted to run it as a service, then using `nohup` and a few other methods including screen environments, with no luck. My co-supervisor was able to find a solution that worked and my nodes can now be run uninterrupted.

Finally I had an issue where some of my peers would connect and disconnect rapidly and this would get me temporarily banned from the `whois` service I was using. Once peers were being fetched by my code the \gls{asn} would be extracted from the IP for each one regardless of whether or not the peer had already been added to the database. A quick fix that I was able to implement involves a simple database lookup so that if the peer has already been added to the database, it is then possible to extract the \gls{asn} directly from the database.
